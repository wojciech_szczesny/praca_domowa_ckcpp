#include "cqmina.h"
#include <QBrush>
#include <QRect>
#include <QPainter>
#include <QStyleOption>
#include <QtCore/qglobal.h>



CQMina::CQMina( QGraphicsScene *scena )
{
 mina = new CMina();
 setPos(mina->x, mina->y);
 scena->addItem(this);
}

void CQMina::point()
{
setPos(mina->x, mina->y);
}

QRectF CQMina::boundingRect() const
{
qreal adjust = 0.5;
return QRectF(-18 - adjust, -22 - adjust,
              36 + adjust, 60 + adjust);
}


QPainterPath CQMina::shape() const
{
QPainterPath path;
path.addRect(-10, -20, 20, 40);
return path;
}


void CQMina::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
 painter->setBrush(Qt::red);
//(QPixmap(":/new/prefix1/images/tree.png"));
painter->drawEllipse(0,0,15,15);
}
