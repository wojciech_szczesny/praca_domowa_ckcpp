#include <cwall.h>
#include "cmap.h"
#include "cimmoveable.h"


void CWall::action()
{
    size_t numer_w_kolejce;
    std::deque<CObject *> lista=Mapa->get_object_function();
     for(size_t i=0; i<lista.size();i++){
             if(lista[i]==this){
             numer_w_kolejce=i;
               }
      }

     for( size_t i=0; i<lista.size(); i++){
        if(abs(lista[i]->y-this->y )<this->a && abs(lista[i]->x -this->x)<this->b){
           CImmoveable *sciana=dynamic_cast<CImmoveable*>(lista[i]);
           if(sciana){
               this->HP=-2;
           }
        }
     }

    if(this->HP<0){
         Mapa->delete_object(numer_w_kolejce);
    }
}
CWall::CWall()
{
    this->a=8;
    this->b=40;
    this->HP=10;
}
