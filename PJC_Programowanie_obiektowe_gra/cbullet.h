#ifndef CBULLET_H
#define CBULLET_H
/**
* \file cbullet.h
* \brief Plik nagłówkowy modułu CBullet.
*
*/

#include "cmoveable.h"
/** \brief Klasa CBullet.
 *
* Pocisk po trafieniu w przeciwnika zmiejsza jego HP. Pociski nie usuwają siebie nawazjem oraz nie zmniejszaja HP jednostek tej samej strony konfliktu.
* Zostają zniszczone po wyjściu poza plansze lub przekroczeniu zasiegu.
*/


class CBullet : public CMoveable
{
public:
    /**\brief Metoda odpowiedzialna za poruszanie
     *
     * Pociski Amerykanów poruszają się w dół.
     * Pociski Talibów poruszają się w górę.
     *
     */
    void move();
    /**\brief Metoda odpowiedzialna za podejmowanie akcji
     *
     * Metoda ta odpowiada za podejmowanie akcji:
     * -poruszanie(pocisk porusza sie z okresloną predkoscia)
     * -decyduje o usunieciu pocisku, który wyszedł poza plansze, trafił w przeciwnika lub budynek oraz zmiejsza ich HP, przekroczył zasieg
     *
     */
    void action();
    CBullet();
    ~CBullet();
};

#endif // CBULLET_H
