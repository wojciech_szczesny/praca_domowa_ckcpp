#ifndef COBJECT_H
#define COBJECT_H
#include <QGraphicsItem>

class CMap;
/** \brief Klasa CObject
 *
 * Klasa bazowa obiektów poruszających się i nieporuszjących się.
 *
*/
class CObject
{
public:
    ///współrzędna x
    int x;
    /// współrzędna y
    int y;
    /// strona_konfliktu, czy talibowie, czy amerykanie
   char strona_konfliktu;
   virtual void action ();
    CMap *Mapa;
    CObject();
    ///metoda ustawiająca mapę
    void set_Mapa(CMap*);
 };


#endif // COBJECT_H
