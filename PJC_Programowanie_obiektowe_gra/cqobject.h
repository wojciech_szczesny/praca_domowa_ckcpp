#ifndef CQOBJECT_H
#define CQOBJECT_H
#include <QGraphicsItem>
#include <QObject>

/** \brief Klasa CQObject Klasa bazowa obiektów graficznych.
 *
*/
class CQObject
{
public:
    virtual void point()=0;

public:
    CQObject();
   // void set_Mapa();
    virtual ~CQObject();

    QRectF boundingRect() ;
    QPainterPath shape();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);
};

#endif // CQOBJECT_H
