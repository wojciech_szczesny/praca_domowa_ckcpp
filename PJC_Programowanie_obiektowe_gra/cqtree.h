#ifndef CQTREE_H
#define CQTREE_H

#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QObject>
#include <cqobject.h>
#include "ctree.h"
#include <QGraphicsPixmapItem>

/** \brief Klasa CQTree.
 *
* Klasa graficzna drzewa.
*/
class CQTree :  public QGraphicsPixmapItem, public CQObject
{
public:
    CQTree(QGraphicsScene*);
    /// Wskaźnik na element logiczny CTree
    CTree *drzewo;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();


};

#endif // CQTANK_H
