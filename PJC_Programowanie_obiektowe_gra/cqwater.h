#ifndef CQWATER_H
#define CQWATER_H

#include <QGraphicsScene>
#include <cqobject.h>
#include "cwater.h"
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>

/** \brief Klasa CQWater
 *
* Klasa graficzna wody.
*/
class CQWater :  public CQObject, public QGraphicsPixmapItem
{
public:
    CQWater(QGraphicsScene*);
    /// Wskaźnik na element logiczny CWater
    CWater *woda;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();
};

#endif // CQWATER_H
