#include "cqtalibsnajper.h"
#include <QBrush>
#include <QRect>
#include <QPainter>
#include <QStyleOption>


CQTalibSnajper::CQTalibSnajper( QGraphicsScene *scena )
{
    soldier = new CTalibSnajper();
     setPos(soldier->x, soldier->y);
     scena->addItem(this);

}

CQTalibSnajper::~CQTalibSnajper()
{

}


void CQTalibSnajper::point()
{
    setPos(soldier->x, soldier->y);
}

QRectF CQTalibSnajper::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust,
                  36 + adjust, 60 + adjust);
}


QPainterPath CQTalibSnajper::shape() const
{
    QPainterPath path;
    path.addRect(-5, -10, 10, 20);
    return path;
}


void CQTalibSnajper::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
     painter->setBrush(Qt::blue);
    painter->drawEllipse(-5, -10, 10, 20);
}




