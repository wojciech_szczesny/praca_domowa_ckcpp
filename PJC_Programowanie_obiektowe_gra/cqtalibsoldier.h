#ifndef CQTALIBSOLDIER_H
#define CQTALIBSOLDIER_H
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QObject>
#include <cqobject.h>
#include "ctalibsoldier.h"
/** \brief Klasa CQTalibSoldier.
 *
* Klasa graficzna talibskiego soldiera. Na mapie czarna elipsa.
*/
class CQTalibSoldier  : public QGraphicsItem, public CQObject
{
public:
    CQTalibSoldier(QGraphicsScene *);
    ~CQTalibSoldier();
    /// Wskaźnik na element logiczny CTalibSoldier
    CTalibSoldier *soldier;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;
};

#endif // CQTALIBSOLDIER_H
