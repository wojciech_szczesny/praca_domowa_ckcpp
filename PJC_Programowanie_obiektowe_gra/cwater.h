#ifndef CWATER_H
#define CWATER_H
#include "cimmoveable.h"

/** \brief Klasa CWater.
 *
* Jedyną jednostką, która jest w stanie przejechać przez wodę jest CTank.
* Woda nie wykonuje żadnej akcji.
*/

class CWater : public CImmoveable
{
public:
    void action();
    CWater();
};

#endif // CWATER_H
