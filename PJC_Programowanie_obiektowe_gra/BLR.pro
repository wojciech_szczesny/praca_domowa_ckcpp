#-------------------------------------------------
#
# Project created by QtCreator 2018-04-29T20:43:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BLR
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    game.cpp \
    cmap.cpp \
    cobject.cpp \
    cimmoveable.cpp \
    ctree.cpp \
    cmoveable.cpp \
    ctank.cpp \
    cbullet.cpp \
    ctalibsoldier.cpp \
    cmina.cpp \
    cwater.cpp \
    cbuilding.cpp \
    cwall.cpp \
    chouse.cpp \
    cqtank.cpp \
    cqobject.cpp \
    cqbullet.cpp \
    cqtalibsoldier.cpp \
    cqtree.cpp \
    cqwater.cpp \
    cqmina.cpp \
    cqhouse.cpp \
    camericansoldier.cpp \
    cqamericansoldier.cpp \
    ctalibsnajper.cpp \
    cqtalibsnajper.cpp \
    camericansaper.cpp \
    cqamericansaper.cpp \
    cqwall.cpp

HEADERS += \
        mainwindow.h \
    game.h \
    cmap.h \
    cobject.h \
    cimmoveable.h \
    ctree.h \
    cmoveable.h \
    ctank.h \
    cbullet.h \
    ctalibsoldier.h \
    cmina.h \
    cwater.h \
    cbuilding.h \
    cwall.h \
    chouse.h \
    cqtank.h \
    cqobject.h \
    cqbullet.h \
    cqtalibsoldier.h \
    cqtree.h \
    cqwater.h \
    cqmina.h \
    cqhouse.h \
    camericansoldier.h \
    cqamericansoldier.h \
    ctalibsnajper.h \
    cqtalibsnajper.h \
    cqamericansaper.h \
    cqwall.h \
    camericansaper.h

FORMS += \
        mainwindow.ui

RESOURCES += \
    grafika.qrc
