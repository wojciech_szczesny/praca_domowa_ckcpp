#include "cqtank.h"
#include <QBrush>
#include <QPainter>
#include <QStyleOption>
#include <QtCore/qglobal.h>
#include <QTime>

CQTank::CQTank( QGraphicsScene *scena )
{
     czolg = new CTank();
     setPos(czolg->x, czolg->y);
     scena->addItem(this);
}

void CQTank::point()
{
    setPos(czolg->x, czolg->y);
}

QRectF CQTank::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust,
                  36 + adjust, 60 + adjust);
}


QPainterPath CQTank::shape() const
{
   QPainterPath path;
   path.addRect(-30, -30, 30, 30);
   return path;
}


void CQTank::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::yellow);
    painter->drawEllipse(-10,-10,20,20);
}



CQTank::~CQTank()
{

}


