/**
* \mainpage
* Projekt PJC - "ISIS"
* \author Wojciech Szczęsny
* \date 07.06.2018r.
* \version 1.0
* \par Kontakt:
* \a wojciech.szczesny.poczta@gmail.com
*/

#ifndef GAME_H
#define GAME_H
#include <QTimer>
#include <QObject>

class CMap;
/** \brief Klasa CBullet.
 *
 * Główna klasa programu odpowiedzialna za tworzenie jednostek i budynków, oraz za symulację
 *
*/

class CGame : public QObject{
    Q_OBJECT

public:

     CGame();
     ///atrybut za pomocą którego uruchamiana jest i kończona symulacja
      QTimer *timer;
      /// Mapa
     CMap *Mapa;
     /// Metoda odpowiedzialna za tworzenie obiektów nieruchomych oraz jednostek wojskowych
     void create(CMap *);
     friend class CMap;
     friend void set_Mapa( CGame *);

public slots:
     /// Metoda odpowiedzialna, za symulację.
     void simulatestep();



};
#endif // GAME_H

