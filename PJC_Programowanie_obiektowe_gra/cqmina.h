#ifndef CQMINA_H
#define CQMINA_H


#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <cqobject.h>
#include "cmina.h"
/** \brief Klasa CQMina
 *
* Klasa graficzna miny. Na mapie duże czerwone kółko.
*/

class CQMina : public QGraphicsItem, public CQObject
{
public:
    CQMina(QGraphicsScene*);
    /// Wskaźnik na element logiczny CMina
    CMina *mina;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;
};


#endif // CQMINA_H
