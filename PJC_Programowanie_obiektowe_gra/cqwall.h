#ifndef CQWALL_H
#define CQWALL_H


#include <QGraphicsItem>
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <cqobject.h>
#include "cwall.h"
/** \brief Klasa CQWall
 *
* Klasa graficzna sciany/ogrodzenia. Reprezentowana białym wąskim prostokątem.
*/

class CQWall : public QGraphicsItem, public CQObject
{
public:
    CQWall(QGraphicsScene*);
    /// Wskaźnik na element logiczny CWall
    CWall *sciana;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;
};


#endif // CQWALL_H
