#ifndef CQTALIBSNAJPER_H
#define CQTALIBSNAJPER_H
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QObject>
#include <cqobject.h>
#include "ctalibsnajper.h"

/** \brief Klasa CQTalibSnajper.
 *
* Klasa graficzna talibskiego snajpera. Na mapie niebieska elipsa.
*/
class CQTalibSnajper : public QGraphicsItem, public CQObject
{
public:
    CQTalibSnajper(QGraphicsScene *);
    ~CQTalibSnajper();
    ///Wskaźnik na element logiczny CTalibSnajper
    CTalibSnajper *soldier;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;

};

#endif // CQTALIBSNAJPER_H
