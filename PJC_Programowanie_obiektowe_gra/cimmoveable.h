#ifndef CIMMOVEABLE_H
#define CIMMOVEABLE_H
#include "cobject.h"
/** \brief Klasa CImmovable.
 *
 * Klasa bazowa dla wszystkich nieruchomych obiektów.
*/

class CImmoveable : public CObject
{
public:
    virtual void action ();
    CImmoveable();
};



#endif // CIMMOVEABLE_H
