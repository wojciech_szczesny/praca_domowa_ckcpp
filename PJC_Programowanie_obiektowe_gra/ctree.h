#ifndef CTREE_H
#define CTREE_H
#include "cimmoveable.h"
/** \brief Klasa CTree.
 *
 * Drzewo nie wykonuje żadnej akcji. Tworzy scenografię planszy.
*/


class CTree : public CImmoveable
{
public:
    /// Metoda nie wykonuje żadnej akcji.
    void action();
    CTree();
};

#endif // CTREE_H
