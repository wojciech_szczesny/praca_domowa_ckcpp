#include "cqbullet.h"
#include <QGraphicsView>
#include <QBrush>
#include <QGraphicsRectItem>
#include <QPainter>
#include <QStyleOption>
#include <QtCore/qglobal.h>



void CQBullet::point()
{
    setPos(pocisk->x, pocisk->y);
}

CQBullet::CQBullet()
{
    pocisk = new CBullet;
    setPos(pocisk->x, pocisk->y);
    setRect(0,0,10,40);
}

CQBullet::~CQBullet()
{

}

QRectF CQBullet::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-1 - adjust, -2 - adjust,
                  1 + adjust, 2 + adjust);
}


QPainterPath CQBullet::shape() const
{
    QPainterPath path;
    path.addRect(-4, -5, 5, 4);
    return path;
}


void CQBullet::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

     painter->setBrush(Qt::red);
    painter->drawEllipse(-4, -5, 5, 4);

}
