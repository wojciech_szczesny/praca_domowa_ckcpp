#ifndef CTALIBSNAJPER_H
#define CTALIBSNAJPER_H


#include "cmoveable.h"
#include "cbuilding.h"
#include "cwater.h"

/** \brief Klasa CTalibSnajper.
 *
* Klasa logiczna talibskiego snajpera.
*/
class CTalibSnajper : public CMoveable
{
    /**\brief Metoda omijająca budynek
     *
     * Metoda odpowiada, za sterowanie obiektem, gdy ten natrafi na budynek
     * Metoda działa następujący sposób:
     * jeżeli obiekt nadjedzie z górnego lewego rogu to będzie poruszał się w lewo oraz do góry
     * jeżeli obiekt nadjedzie z górnego prawego rogu to będzie poruszał się w prawą stronę oraz do góry
     * jeżeli obiekt nadjedzie z dolnego prawego rogu to będzie poruszał się w prawo oraz do dołu
     * jeżeli obiekt nadjedzie z dolnego lewego rogu to będzie poruszał się  w lewo oraz do dołu
     */
    void wykrytobudynek( CBuilding* );
    /**\brief Metoda omijająca wodę
     *
     * Metoda odpowiada, za sterowanie obiektem, gdy ten natrafi na wodę
     * Metoda działa następujący sposób:
     * jeżeli obiekt nadjedzie z górnego lewego rogu to będzie poruszał się w lewo oraz do góry
     * jeżeli obiekt nadjedzie z górnego prawego rogu to będzie poruszał się w prawą oraz do góry
     * jeżeli obiekt nadjedzie z dolnego prawego rogu to będzie poruszał się w prawo oraz do dołu
     * jeżeli obiekt nadjedzie z dolnego lewego rogu to będzie poruszał się  w lewo oraz do dołu
     */
    void wykrytowode(CWater* );
public:
    /**\brief Metoda odpowiedzialna za poruszanie
     *
     * Jeżeli obiekt znajduje się 100 pikseli od górnej lub dolnej krawedzi to porusza się do środka,
     * w przeciwnym razie (gdy y znajduje sie w obszarze <100,500>) to jest losowane czy poruszy się do góry, do dołu, czy pozostanie w miejscu.
     *
     * Metoda na podstawie kolejki obiektów przeciwnika znajdujących się w zasiegu obiektu, znajduje przeciwnika będącego najbliżej (współrzedna x) i w jego kierunku wykonuje ruch.
     * Jeżeli najbliższy przeciwnik i CAmericanSaper mają taka samą współrzędną x wtedy obiekt nie poruszy się ani w prawo, ani w lewo.
     *
     * Jeżeli w zasiegu obiektu nie bedzie zadnego przeciwnika wtedy ruch do środka mapy następuje gdy obiekt znajdzie się w odleglosci mniejszej niż 50 pikseli od mapy.
     * Gdy jego odlegosc od lewej i prawej strony jest wieksza niż 50 wtedy następuje losowanie, obiekt może poruszyć się w lewo, w prawo lub pozostać w miejscu.
     */
    void move();
    /**\brief Metoda odpowiedzialna za podejmowanie akcji
     *
     * Metoda ta odpowiada za podejmowanie akcji:
     *jezeli metoda wykryje, że obiekt natrafił na wodę wtedy uruchamiana jest metoda odpowiedzialna za ominięcie wody,
     *jeżeli metoda wykryje, że obiekt natrafił na domek wtedy uruchamiana jest metoda odpowiedzialna za ominięcie domka,
     *jeżeli metoda nie wykryła przeszkody(domek, woda) to wtedy uruchamiana jest metoda odpowiedzialna za "zwykłe" poruszanie,
     * Metoda ta również decyduje kiedy zostanie oddany strzał (kiedy w zasięgu obiektu znajduje się przeciwnik oraz upłynął minimalny czas od poprzedniego wystrzału).
     *Metoda ta sprawdza również, jaki jest stan HP, jeżeli jest mniejszy niż 0, wtedy obiekt zostanie usunięty.
     */
    void action()  override;
    /**\brief Metoda odpowiedzialna za strzelanie
     *
     * Metoda ta tworzy pocisk oraz przypisuje mu:
     * - stronę konfliktu (pocisk niszczy jedynie obiekty przeciwnika),
     * -zasieg (po jego przekroczeniu pocisk zostanie zniszczony),
     * -obrazenia, jakie zada przeciwnikowi po trafieniu
     */
    void shoot()    override;
    CTalibSnajper();
};



#endif // CTALIBSNAJPER_H
