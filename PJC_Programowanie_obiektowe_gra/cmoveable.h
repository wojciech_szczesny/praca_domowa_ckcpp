#ifndef CMOVEABLE_H
#define CMOVEABLE_H
#include "cobject.h"

/** \brief Klasa CMoveable.
 *
 * Klasa bazowa obiektów poruszających się.
*/
class CMoveable :public CObject
{
public:
    CMoveable();
    /// odleglość, jaką może pokonać pocisk tego obiektu, oraz maksymalna odleglosc wykrywania wrogich jednostek
    int zasieg;
    /// predkosc obiektu
    int predkosc;
    /// czas przeladowania(mozliwosc wystrzelnia nowego pocisku)
    int czas_tworzenia;
    /// stan zdrowia jednoski wojskowej
    int HP;
    /// obrazenia jakie spowoduje pocisk jednostki
    int obrazenia;
    virtual void move();
    virtual void action() override;
    virtual void shoot();
    virtual ~CMoveable();
};

#endif // CMOVEABLE_H
