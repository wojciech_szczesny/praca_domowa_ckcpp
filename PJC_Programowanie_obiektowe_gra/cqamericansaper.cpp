#include "cqamericansaper.h"
#include <QDebug>
#include <QGraphicsRectItem>
#include <QGraphicsView>
#include <QBrush>
#include <QPainter>
#include <QStyleOption>
#include <QtCore/qglobal.h>


CQAmericanSaper::CQAmericanSaper( QGraphicsScene *scena )
{
     soldier = new CAmericanSaper();
     setPos(soldier->x, soldier->y);
     scena->addItem(this);
}

void CQAmericanSaper::point()
{
    setPos(soldier->x, soldier->y);
}

QRectF CQAmericanSaper::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust,
                  36 + adjust, 60 + adjust);
}


QPainterPath CQAmericanSaper::shape() const
{
   QPainterPath path;
   path.addRect(-10, -20, 20, 40);
   return path;
}


void CQAmericanSaper::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::red);
    painter->drawEllipse(-5, -5, 10, 10);
}



CQAmericanSaper::~CQAmericanSaper()
{

}



