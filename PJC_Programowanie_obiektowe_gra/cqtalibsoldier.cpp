#include "cqtalibsoldier.h"
#include <QBrush>
#include <QRect>
#include <QPainter>
#include <QStyleOption>


CQTalibSoldier::CQTalibSoldier( QGraphicsScene *scena )
{
    soldier = new CTalibSoldier();
     setPos(soldier->x, soldier->y);
     scena->addItem(this);
}

CQTalibSoldier::~CQTalibSoldier()
{

}


void CQTalibSoldier::point()
{
    setPos(soldier->x, soldier->y);
}

QRectF CQTalibSoldier::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust,
                  36 + adjust, 60 + adjust);
}


QPainterPath CQTalibSoldier::shape() const
{
    QPainterPath path;
    path.addRect(-10, -20, 20, 40);
    return path;
}


void CQTalibSoldier::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

     painter->setBrush(Qt::black);
    painter->drawEllipse(-10, -20, 20, 40);
}



