#ifndef CQHOUSE_H
#define CQHOUSE_H



#include <QGraphicsObject>
#include <QGraphicsScene>
#include <cqobject.h>
#include "chouse.h"
/** \brief Klasa CQHouse.
 *
* Reprezentuje dom(bialy kwadrat).
*/

class CQHouse : public QGraphicsItem, public CQObject
{
public:
    CQHouse(QGraphicsScene*);
    /// Wskaźnik na element logiczny CHouse
    CHouse *dom;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;
};



#endif // CQHOUSE_H
