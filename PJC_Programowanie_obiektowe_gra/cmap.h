#ifndef CMAP_H
#define CMAP_H
#include <deque>
#include <QGraphicsScene>

class CObject;
class CQObject;

/** \brief Klasa CMap.
 *
* Klasa odpowiedzialna za zarządzanie mapą.
*
*/
class CMap
{
    ///szerokosc Mapy
int szerokosc;
///wysokosc Mapy
int wysokosc;
public:
/// scena
  QGraphicsScene * scena;
  CMap();
  /// Kolejka obiektów w zasiegu konkretnego obiektu
  std::deque<CObject *> getInRange;
  /// Funkcja która wyszukuje przeciwników w zasięgu obiektu
  void get_in_range_function(CObject*, int, char);
  /// Kolejka obiektów
  std::deque<CObject *> get_object_function() const;
  /// Kolejka graficznych obiektów
  std::deque<CQObject *> get_gobject_function() const;
  ///Metoda usuwająca obiekty z planszy
  void delete_object(size_t);
  ///Metoda dodająca obiekty na plansze
  void add_object(CQObject*, CObject*);
  friend class CGame;

private:
  ///Lista obiektów na planszy gry
   std::deque <CObject*> obiekty;
   /// Lista obiektów graficznych na planszy gry
   std::deque <CQObject*> gobiekty;
};

#endif // CMAP_H



