#ifndef CQAMERICANSAPER_H
#define CQAMERICANSAPER_H

#include <QGraphicsRectItem>
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QObject>
#include <cqobject.h>
#include "camericansaper.h"

/** \brief Klasa CAmericanSaper.
 *
* Klasa odpowiadająca za grafikę. Na mapie małe czerwone koło.
*/
class CQAmericanSaper : public QGraphicsItem, public CQObject
{
public:
    CQAmericanSaper(QGraphicsScene *);

    ~CQAmericanSaper();
    /// Wskaźnik na element logiczny CAmericanSaper
    CAmericanSaper *soldier;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;
};


#endif // CQAMERICANSAPER_H
