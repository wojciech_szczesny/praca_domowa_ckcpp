#ifndef CMINA_H
#define CMINA_H
#include "cimmoveable.h"

/** \brief Klasa CMina.
 *
* Klasa niszczy ruchome jednostki(nie niszczy pocisków i sapera).
*/
class CMina : public CImmoveable
{
public:
    /// Wartość o jaką zmiejszy się HP jednostki poruszającej się (z wyjątkiem CAmericanSaper i CBullet)
    int obrazenia;
    /// Metoda zmniejsza HP jednostki poruszającej się (z wyjątkiem CAmericanSaper i CBullet), gdy obiekt znajdzie się w jej zasięgu
    void action();
    CMina();
};

#endif // CMINA_H
