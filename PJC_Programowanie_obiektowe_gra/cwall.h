#ifndef CWALL_H
#define CWALL_H
#include "cbuilding.h"

/** \brief Klasa CWall
 *
* Ścianę można łatwo  zburzyć za pomocą pocisku lub przejazdu jednostki ruchomej.
* Nie wykonuje żadnej akcji.
*/

class CWall : public CBuilding
{
public:
    void action();
    CWall();
};

#endif // CWALL_H
