#include "game.h"
#include <QDebug>
#include "ctalibsnajper.h"
#include <QTimer>
#include "cmap.h"
#include <iostream>
#include "ctank.h"
#include "cqtree.h"
#include "cqtank.h"
#include "cqtalibsoldier.h"
#include "ctree.h"
#include "cwater.h"
#include "cqwater.h"
#include "cmoveable.h"
#include "ctalibsoldier.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include "cmina.h"
#include "cqmina.h"
#include "chouse.h"
#include "cqhouse.h"
#include "cqtalibsnajper.h"
#include "cqamericansoldier.h"
#include "cqwall.h"
#include "cqamericansaper.h"

void CGame::simulatestep()
{
    int licznik1=0;
    int licznik2=0;

    for( size_t i=0; i<Mapa->obiekty.size(); i++){
       Mapa->obiekty[i]->action();
    }

    for( size_t j=0; j<Mapa->gobiekty.size(); j++){

         if(Mapa->obiekty[j]->strona_konfliktu=='A'){
             licznik1++;
         }else if(Mapa->obiekty[j]->strona_konfliktu=='T'){
             licznik2++;
         }
      Mapa->gobiekty[j]->point( );
      if(licznik1==0 || licznik2==0){
          timer->stop();
      }else
      timer->start(10);
  }
}

CGame::CGame()
{
    Mapa=new CMap();
    this->create(Mapa );
}

void CGame::create(CMap *Mapa)
{
    timer= new QTimer();
    connect(timer, SIGNAL(timeout()), this , SLOT(simulatestep()));
    timer->start(10);

    CQObject *gobiekt4 =new CQWater(Mapa->scena);
    CQWater *gwater = dynamic_cast<CQWater*>(gobiekt4);
    gwater->woda->set_Mapa(Mapa);
    Mapa->add_object(gwater,gwater->woda);

    CQTree *gdrzewo = new CQTree(Mapa->scena);
    gdrzewo->drzewo->set_Mapa(Mapa);
    gdrzewo->drzewo->x=550;
    gdrzewo->drzewo->y=170;
    Mapa->add_object(gdrzewo, gdrzewo->drzewo);

    CQTree *gdrzewo2 = new CQTree(Mapa->scena);
    gdrzewo2->drzewo->set_Mapa(Mapa);
    gdrzewo2->drzewo->x=620;
    gdrzewo2->drzewo->y=200;
    Mapa->add_object(gdrzewo2, gdrzewo2->drzewo);

    CQTree *gdrzewo3 = new CQTree(Mapa->scena);
    gdrzewo3->drzewo->set_Mapa(Mapa);
    gdrzewo3->drzewo->x=590;
    gdrzewo3->drzewo->y=250;
    Mapa->add_object(gdrzewo3, gdrzewo3->drzewo);

    CQObject *gobiekt= new CQTank(Mapa->scena);
    CQTank *gczolg = dynamic_cast<CQTank*>(gobiekt);
    gczolg->czolg->set_Mapa(Mapa);
    Mapa->add_object(gczolg, gczolg->czolg);

    CQObject *gobiekt3= new CQTalibSoldier(Mapa->scena);
    CQTalibSoldier *gtalibsoldier = dynamic_cast<CQTalibSoldier*>(gobiekt3);
    gtalibsoldier->soldier->set_Mapa(Mapa);
    Mapa->add_object(gtalibsoldier,gtalibsoldier->soldier);

    CQObject *gobiekt31= new CQTalibSoldier(Mapa->scena);
    CQTalibSoldier *gtalibsoldier1 = dynamic_cast<CQTalibSoldier*>(gobiekt31);
    gtalibsoldier1->soldier->set_Mapa(Mapa);
    Mapa->add_object(gtalibsoldier1,gtalibsoldier1->soldier);

    CQObject *gobiekt5 =new CQMina(Mapa->scena);
    CQMina *gmina= dynamic_cast<CQMina*>(gobiekt5);
    gmina->mina->set_Mapa(Mapa);
    Mapa->add_object(gmina,gmina->mina);

    CQObject *gobiekt6 =new CQHouse(Mapa->scena);
    CQHouse *ghouse= dynamic_cast<CQHouse*>(gobiekt6);
    ghouse->dom->set_Mapa(Mapa);
    ghouse->dom->x=130;
    ghouse->dom->y=350;
    Mapa->add_object(ghouse,ghouse->dom);

    CQObject *gobiekt16 =new CQHouse(Mapa->scena);
    CQHouse *ghouse2= dynamic_cast<CQHouse*>(gobiekt16);
    ghouse2->dom->set_Mapa(Mapa);
    ghouse2->dom->x=230;
    ghouse2->dom->y=400;
    Mapa->add_object(ghouse2,ghouse2->dom);

    CQObject *gobiekt8 =new CQAmericanSoldier(Mapa->scena);
    CQAmericanSoldier *gasoldier= dynamic_cast<CQAmericanSoldier*>(gobiekt8);
    gasoldier->soldier->set_Mapa(Mapa);
    Mapa->add_object(gasoldier,gasoldier->soldier);

    CQObject *gobiekt111= new CQTalibSnajper(Mapa->scena);
    CQTalibSnajper *gtalibsnajper111 = dynamic_cast<CQTalibSnajper*>(gobiekt111);
    gtalibsnajper111->soldier->set_Mapa(Mapa);
    Mapa->add_object(gtalibsnajper111,gtalibsnajper111->soldier);

    CQObject *gobiekt12=new CQWall(Mapa->scena);
    CQWall *gwall=dynamic_cast<CQWall*>(gobiekt12);
    gwall->sciana->set_Mapa(Mapa);
    gwall->sciana->x=100;
    gwall->sciana->y=300;
    Mapa->add_object(gwall,gwall->sciana);

    CQObject *gobiekt13=new CQWall(Mapa->scena);
    CQWall *gwall2=dynamic_cast<CQWall*>(gobiekt13);
    gwall2->sciana->set_Mapa(Mapa);
    gwall2->sciana->x=150;
    gwall2->sciana->y=300;
    Mapa->add_object(gwall2,gwall2->sciana);

    CQObject *gobiekt14=new CQWall(Mapa->scena);
    CQWall *gwall3=dynamic_cast<CQWall*>(gobiekt14);
    gwall3->sciana->set_Mapa(Mapa);
    gwall3->sciana->x=190;
    gwall3->sciana->y=300;
    Mapa->add_object(gwall3,gwall3->sciana);

    CQObject *gobiekt123=new CQAmericanSaper(Mapa->scena);
    CQAmericanSaper *gsaper=dynamic_cast<CQAmericanSaper*>(gobiekt123);
    gsaper->soldier->set_Mapa(Mapa);
    Mapa->add_object(gsaper,gsaper->soldier);
}



