#include "cmap.h"
#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsView>
#include "cqobject.h"
#include "cobject.h"
#include "cbullet.h"

CMap::CMap()
{
    this->szerokosc=800;
    this->wysokosc=600;

    scena = new QGraphicsScene();
    scena->setSceneRect(0,0, 800, 600);

     QGraphicsView *view = new QGraphicsView(scena);
     view->setBackgroundBrush(QPixmap(":/new/prefix1/images/tlo.jpg"));
     view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
     view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
     view->show();
     view->setFixedSize(this->szerokosc,this->wysokosc);
}

void CMap::get_in_range_function(CObject *element, int zasieg, char znak)
{
    getInRange.clear();
    //zlecajacy to znak, a funkcja ma przeciwnikow zlecajacego zwrocic
    char tmp;
    if(znak=='A'){
        tmp='T';
    }else
        tmp='A';
    for( size_t i=0; i<this->obiekty.size(); i++){

        CBullet* bullet = dynamic_cast<CBullet*>(obiekty[i]);
        if(!bullet){
                if(obiekty[i]->strona_konfliktu==tmp && abs(obiekty[i]->y-element->y)<=zasieg && abs(obiekty[i]->x-element->x)<=zasieg){
               // qDebug() << "przeciwnik sie zbliza: " << abs(obiekty[i]->y-element->y) << endl;
                getInRange.push_back(obiekty[i]);

       //qDebug() << "W poblizu jest pokemon: " << getInRange.size() << endl;
        }
        }
    }
}

std::deque<CObject *> CMap::get_object_function() const
{
    std::deque<CObject *> kolejka;
    for( size_t i=0; i<this->obiekty.size(); i++){
               kolejka.push_back(obiekty[i]);
    }
    return kolejka;
}

std::deque<CQObject *> CMap::get_gobject_function() const
{
    std::deque<CQObject *> kolejka;
    for( size_t i=0; i<this->gobiekty.size(); i++){
               kolejka.push_back(gobiekty[i]);
    }
    return kolejka;
}

void CMap::delete_object(size_t numer)
{
    CQObject *tmp=this->gobiekty[numer];
    obiekty.erase(obiekty.begin()+numer);
    gobiekty.erase(gobiekty.begin()+numer);
    delete tmp;
}

void CMap::add_object( CQObject  *dodaj_gobiekt, CObject *dodaj_obiekt)
{
    gobiekty.push_back(dodaj_gobiekt);
    obiekty.push_back(dodaj_obiekt);
}
