#include "cbullet.h"
#include <QDebug>
#include <QGraphicsScene>
#include "cqobject.h"
#include "cbuilding.h"
#include "cmap.h"



void CBullet::move()
{
    //dekrementujemy zasieg, max odl przelotu pocisku
    this->zasieg=this->zasieg-1;
    //poruszanie, jedni w gore, drudzy do dolu
    if(this->strona_konfliktu=='A'){
    this->y=this->y+1;
   // qDebug() <<"POCISK MOVE: Pozycja pocisku x:" << this->x << "Pozycja pocisku y: " << this->y;
    }
    else{
        this->y=this->y-1;
        //qDebug() <<"POCISK MOVE TALIB: Pozycja pocisku x:" << this->x << "Pozycja pocisku y: " << this->y;
    }
}

void CBullet::action()
{
    size_t numer_w_kolejce;
    std::deque<CObject *> lista=Mapa->get_object_function();
     for(size_t i=0; i<lista.size();i++){
             if(lista[i]==this){
             numer_w_kolejce=i;
               }
      }

    this->predkosc=this->predkosc-1;
    if(this->predkosc==0){
    this->move();
        this->predkosc=1;
    }
//usuwamy poza plansza
    if(this->y>600 || this->y<0){
        for(size_t i=0; i<lista.size();i++){
             if(lista[i]==this){
                  Mapa->delete_object(i);
            }
         }
    }
    //usuwamy jak uderzy w czolg
    char znak;
    if (this->strona_konfliktu=='A'){
        znak='T';
    }else
        znak='A';

    // USUWAMY BULLET i ZMNIEJSZAMY HP TANKA
    for( size_t i=0; i<lista.size(); i++){
        //przeszukujemy tylko obiekty przeciwnika
         if(lista[i]->strona_konfliktu==znak ){
             // sprawdzamy czy pozycja Tanka i obiekt[i] jest +/-3 taka sama.
               if(abs(this->x-lista[i]->x) <10 && abs(this->y-lista[i]->y)<10){
                           CMoveable *obiekt=dynamic_cast<CMoveable*>(lista[i]);
                           CBuilding *budynek=dynamic_cast<CBuilding*>(lista[i]);
                           //pociski sie nie usuwaja jeden drugiego
                           CBullet *bullet=dynamic_cast<CBullet*>(lista[i]);
                           if(!bullet){
                            if(obiekt){
                                 obiekt->HP=obiekt->HP-this->obrazenia;
                            }
                            if(budynek){
                                budynek->HP=budynek->HP-this->obrazenia;
                                qDebug()<< "BUUUUDDDYYYNNEEKK" << endl;
                            }
                             //usuwamy pociski
                             Mapa->delete_object(numer_w_kolejce);
                            break;
                           }
               }
         }
    }
    if(zasieg<0){
        Mapa->delete_object(numer_w_kolejce);
    }


}


CBullet::CBullet()
{

    this->zasieg=200;
    this->predkosc=3;
    this->obrazenia = 1;
    this->y=300;

}

CBullet::~CBullet()
{

}



