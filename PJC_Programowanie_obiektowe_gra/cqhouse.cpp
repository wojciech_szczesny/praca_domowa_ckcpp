#include "cqhouse.h"
#include <QBrush>
#include <QRect>
#include <QPainter>
#include <QStyleOption>
#include <QtCore/qglobal.h>
#include <QPainter>


CQHouse::CQHouse( QGraphicsScene *scena )
{
     dom = new CHouse();
     setPos(dom->x, dom->y);
     scena->addItem(this);
}

void CQHouse::point()
{
    setPos(dom->x, dom->y);
}

QRectF CQHouse::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust,
                  36 + adjust, 60 + adjust);
}


QPainterPath CQHouse::shape() const
{
    QPainterPath path;
    path.addRect(-20, -20, 40, 40);
    return path;
}


void CQHouse::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::white);
    painter->drawRect(-20, -20, 40, 40);
}
