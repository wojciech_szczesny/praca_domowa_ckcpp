#ifndef CHOUSE_H
#define CHOUSE_H
#include "cbuilding.h"
/** \brief Klasa CHouse.
 *
*/

class CHouse : public CBuilding
{
public:
    /**\brief Metoda odpowiedzialna za podejmowanie akcji
     *
     * Metoda ta odpowiada za podejmowanie akcji:
     * jezeli HP budynki jest mniejsze od 0 wtedy podejmowana jest decyzja o jego usunieciu
     *
     */
    void action();
    CHouse();
};

#endif // CHOUSE_H
