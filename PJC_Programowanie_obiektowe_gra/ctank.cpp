#include "ctank.h"
#include <QDebug>
#include "cbullet.h"
#include "cmap.h"
#include "cqbullet.h"
#include "QGraphicsScene"
#include "cwater.h"
#include "cbuilding.h"


void CTank::move()
{
     CObject *najblizszy_przeciwnik=new CObject();
     int min=600;
     if(this->y<200 )
         this->y=this->y+1;
     else if(this->y>=200 && this->y<400){
        int tmp= -1+ rand( )%(1+1-(-1)); //losowanie z przedzialy -1,1 calkowite
        this->y=this->y+tmp;
     }
     else if(this->y>=400){
        this->y=this->y-1;
     }
//wyszukamy najblizszego przeciwnika:
   if(Mapa->getInRange.size()!=0){
        for(size_t i=0;i<Mapa->getInRange.size();i++){
            if(abs(this->x-Mapa->getInRange[i]->x)<min){
                min=(this->x-Mapa->getInRange[i]->x);
                najblizszy_przeciwnik->x=Mapa->getInRange[i]->x;
            }
         }
            if(this->x-najblizszy_przeciwnik->x<0){
                this->x=this->x+1;
            }else if(this->x-najblizszy_przeciwnik->x>0){

                this->x=this->x-1;
            }
            else
                this->x=this->x;
    }
    else{
       if(this->x<50){
             this->x=this->x+1;
       }else if(this->x>750){
             this->x=this->x-1;
       }else{

        int tmp= -1+ rand( )%(1+1-(-1)); //losowanie z przedzialy -1,1 calkowite
        this->x=this->x + tmp;
       }
    }
}

void CTank::action()
{
    size_t numer_w_kolejce;
    std::deque<CObject *> lista=Mapa->get_object_function();
     for(size_t i=0; i<lista.size();i++){
             if(lista[i]==this){
             numer_w_kolejce=i;
               }
      }

     int tmp1=0, tmp2=0;
     for( size_t i=0; i<lista.size(); i++){
         if(abs(lista[i]->y-this->y )<45 && abs(lista[i]->x -this->x)<60)
         {
            //woda = dynamic_cast<CWater*>(Mapa->obiekty[i]);
            CWater* wodaX = dynamic_cast<CWater*>(lista[i]);
                if(wodaX){
                    tmp1++;
                   wykrytowode();
                    qDebug() << "weszło w petle UWAGA WODA"  << endl;
                    }
         }
     }



      for( size_t i=0; i<lista.size(); i++){
         if(abs(lista[i]->y-this->y )<30 && abs(lista[i]->x -this->x)<30){
            CBuilding *domek=dynamic_cast<CBuilding*>(lista[i]);
            if(domek){
                tmp2++;
                    wykrytobudynek(domek);
                    qDebug()<<"Wykryto domek" << endl;
            }
         }
      }

     if(tmp1==0 && tmp2==0){
         this->predkosc=this->predkosc-1;
             if(this->predkosc==0){
                    this->predkosc=2;
                    this->move();
                  //  qDebug()<<"NIE Wykryto" << endl;
             }
            //else
              //qDebug()<<"Wykryto" << endl;
            }

    //czolg bedzie strzelał tylko co 20 step
    this->czas_tworzenia=this->czas_tworzenia-1;
    if(this->czas_tworzenia==0){
        //qDebug() << Mapa->gobiekty.size();
        Mapa->get_in_range_function(this, this->zasieg, this->strona_konfliktu);
        if(Mapa->getInRange.size()!=0){
             this->shoot();
                //this->predkosc=20000;
        }else
             this->predkosc=2;
             this->czas_tworzenia=220;
     }
    else{
       // qDebug() << "CZEKAJ: " << this->czas_tworzenia ;
    }

    if(this->HP<0){
        Mapa->delete_object(numer_w_kolejce);
    }
}

void CTank::wykrytobudynek(CBuilding *budynek)
{
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x>=0 && budynek->y-this->y>=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 qDebug() << "GÓRA LEWY";
                // this->y=this->y-1;
                 this->x=this->x-1;
                 this->predkosc=2;
             }
    }
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x<=0 && budynek->y-this->y>=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 qDebug() << "GÓRA Prawy";
                // this->y=this->y-10;
                 this->x=this->x+1;
                 this->predkosc=2;
             }
    }
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x<=0 && budynek->y-this->y<=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 qDebug() << "DÓŁ prawy";
                 this->y=this->y+1;
                 this->x=this->x+1;
                 this->predkosc=2;
             }
    }
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x>=0 && budynek->y-this->y<=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 this->y=this->y+1;
                 qDebug() << "dół LEWY";
                 this->x=this->x-1;
                 this->predkosc=2;
             }
    }
}

void CTank::wykrytowode()
{
    this->predkosc=this->predkosc-1;
    qDebug() <<this->predkosc << "  UWAGA WODA" << endl;
    if(this->predkosc<=0){
         this->predkosc=100;
        this->move();
    }
}


void CTank::shoot( )
{
    CQBullet *gpocisk=new CQBullet();
    gpocisk->pocisk->set_Mapa(this->Mapa);
    gpocisk->pocisk->x=this->x;
    gpocisk->pocisk->y=this->y;
    gpocisk->pocisk->strona_konfliktu='A';
    gpocisk->pocisk->zasieg=this->zasieg;
    gpocisk->pocisk->obrazenia=this->obrazenia;
    Mapa->add_object(gpocisk,gpocisk->pocisk);
    Mapa->scena->addItem(gpocisk);
}

CTank::CTank( )
{
    int random_number =rand() % 700;
    this->x=random_number;
    this->y=50;
    this->strona_konfliktu='A';
    this->zasieg=300;
    this->predkosc=2;
    this->czas_tworzenia=400;
    this->HP=50;
    this->obrazenia=6;
}

