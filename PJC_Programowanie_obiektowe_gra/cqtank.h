#ifndef CQTANK_H
#define CQTANK_H
#include <QGraphicsItem>
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QObject>
#include <cqobject.h>
#include "ctank.h"
/** \brief Klasa CQTank
 *
* Klasa graficzna amerykanskiego czoły. Na mapie duże żółte koło.
*/
class CQTank : public QGraphicsItem, public CQObject
{
public:

    CQTank(QGraphicsScene *);

    ~CQTank();
    /// Atrybut elementu logicznego CTank
    CTank *czolg;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;


};

#endif // CQTANK_H
