#ifndef CQAMERICANSOLDIER_H
#define CQAMERICANSOLDIER_H

#include <QGraphicsObject>
#include <QGraphicsScene>
#include <cqobject.h>
#include "camericansoldier.h"

/** \brief Klasa CQAmericanSoldier.
 *
* Klasa graficzna CQAmericanSoldier. Na mapie małe, żółte kółko.
*/
class CQAmericanSoldier : public QGraphicsItem, public CQObject
{
public:
    CQAmericanSoldier(QGraphicsScene *);

    ~CQAmericanSoldier();
    /// Wskaźnik na element logiczny CAmericanSoldier
    CAmericanSoldier *soldier;
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;

};

#endif // CQAMERICANSOLDIER_H
