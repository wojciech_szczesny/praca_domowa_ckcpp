
#include "camericansaper.h"
#include <QDebug>
#include "cbullet.h"
#include "cmina.h"
#include "cmap.h"
#include <QGraphicsScene>
#include "cqbullet.h"
#include "cwater.h"
#include <cstdlib>
#include <ctime>
#include "chouse.h"


void CAmericanSaper::move()
{
   srand( time( NULL ));
   //qDebug() <<"TANK MOVE: Pozycja x:" << this->x << "Pozycja y: " << this->y;
   CObject *najblizszy_przeciwnik=new CObject();
   int min=600;
   if(this->y<200 )
       this->y=this->y+1;
   else if(this->y>=200 && this->y<=400){
      int tmp= -1+ rand( )%(1+1-(-1)); //losowanie z przedzialy -1,1 calkowite
      this->y=this->y+tmp;
     }
     else if(this->y>400){
        this->y=this->y-1;
     }
//wyszukamy najblizszego przeciwnika:

   if(Mapa->getInRange.size()!=0){
        for(size_t i=0;i<Mapa->getInRange.size();i++){
            if(abs(this->x-Mapa->getInRange[i]->x)<min){
                min=(this->x-Mapa->getInRange[i]->x);
                najblizszy_przeciwnik->x=Mapa->getInRange[i]->x;
            }
         }
            if(this->x-najblizszy_przeciwnik->x<0){
                this->x=this->x+1;
            }else if(this->x-najblizszy_przeciwnik->x>0){
                this->x=this->x-1;
            }
            else
                this->x=this->x;
    }
    else{
       if(this->x<50){
             this->x=this->x+1;
       }else if(this->x>750){
             this->x=this->x-1;
       }else{
        int tmp= -1+ rand( )%(1+1-(-1)); //losowanie z przedzialy -1,1 calkowite
        this->x=this->x + tmp;
       }
    }
}

void CAmericanSaper::action()
{
    size_t numer_w_kolejce;
    std::deque<CObject *> lista=Mapa->get_object_function();
     for(size_t i=0; i<lista.size();i++){
             if(lista[i]==this){
             numer_w_kolejce=i;
               }
      }

     int tmp1=0, tmp2=0;
     //qDebug()<< this->predkosc << endl;
     for( size_t i=0; i<lista.size(); i++){
         if(abs(lista[i]->y-this->y )<45 && abs(lista[i]->x -this->x)<60)
         {
            //woda = dynamic_cast<CWater*>(Mapa->obiekty[i]);
            CWater* wodaX = dynamic_cast<CWater*>(lista[i]);
                if(wodaX){
                    tmp1++;
                   wykrytowode(wodaX);
                    qDebug() << "weszło w petle UWAGA WODA"  << endl;
                    }
         }
     }

      for( size_t i=0; i<lista.size(); i++){
         if(abs(lista[i]->y-this->y )<30 && abs(lista[i]->x -this->x)<30){
            CHouse *domek=dynamic_cast<CHouse*>(lista[i]);
            if(domek){
                tmp2++;
                    wykrytobudynek(domek);
                    qDebug()<<"Wykryto domek" << endl;
            }             
            CMina *mina=dynamic_cast<CMina*>(lista[i]);
            if(mina){
                  wykrytomine(i);
            }
         }
      }

     if(tmp1==0 && tmp2==0){
         this->predkosc=this->predkosc-1;
             if(this->predkosc==0){
                    this->predkosc=2;
                    this->move();
                    qDebug()<<"NIE Wykryto" << endl;
             }
            else
              qDebug()<<"Wykryto" << endl;
            }

    //czolg bedzie strzelał tylko co 20 step
    this->czas_tworzenia=this->czas_tworzenia-1;
    if(this->czas_tworzenia==0){
        //qDebug() << Mapa->gobiekty.size();
        Mapa->get_in_range_function(this, this->zasieg, this->strona_konfliktu);
        if(Mapa->getInRange.size()!=0){
       this->shoot();
        //this->predkosc=20000;
        }else
            this->predkosc=2;
        this->czas_tworzenia=220;
     }
    else{
       // qDebug() << "CZEKAJ: " << this->czas_tworzenia ;
    }

    if(this->HP<0){
         Mapa->delete_object(numer_w_kolejce);
    }
}

//Funkcja jest odpowiedzialna za poruszanie obiektu, gdy obiekt napotka na swej drodze budynek.
//Funkcja działa następujący sposób:
//jeżeli obiekt nadjedzie z górnego lewego rogu to będzie poruszał się w lewo oraz do góry
//jeżeli obiekt nadjedzie z górnego prawego rogu to będzie poruszał się w prawą stronę oraz do góry
//jeżeli obiekt nadjedzie z dolnego prawego rogu to będzie poruszał się w prawo oraz do dołu
//jeżeli obiekt nadjedzie z dolnego lewego rogu to będzie poruszał się  w lewo oraz do dołu

void CAmericanSaper::wykrytobudynek(CBuilding *budynek)
{
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x>=0 && budynek->y-this->y>=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 qDebug() << "GÓRA LEWY";
                 this->y=this->y-1;
                 this->x=this->x-1;
                 this->predkosc=2;
             }
    }
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x<=0 && budynek->y-this->y>=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 qDebug() << "GÓRA Prawy";
                 this->y=this->y-1;
                 this->x=this->x+1;
                 this->predkosc=2;
             }
    }
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x<=0 && budynek->y-this->y<=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 qDebug() << "DÓŁ prawy";
                 this->y=this->y+1;
                 this->x=this->x+1;
                 this->predkosc=2;
             }
    }
    if(abs(budynek->x-this->x)<30 && abs(budynek->y-this->y)<30 && budynek->x-this->x>=0 && budynek->y-this->y<=0){
             this->predkosc=this->predkosc-1;
             if(this->predkosc<=0){
                 this->y=this->y+1;
                 qDebug() << "dół LEWY";
                 this->x=this->x-1;
                 this->predkosc=2;
             }
    }
}

void CAmericanSaper::wykrytowode(CWater *woda)
{
    if(abs(woda->x-this->x)<60 && abs(woda->y-this->y)<45 && woda->x-this->x>=0 && woda->y-this->y>=0){
                 this->predkosc=this->predkosc-1;
                 if(this->predkosc<=0){
                     qDebug() << "GÓRA LEWY";
                     this->y=this->y-1;
                     this->x=this->x-1;
                     this->predkosc=2;
                 }
        }
        if(abs(woda->x-this->x)<60 && abs(woda->y-this->y)<45 && woda->x-this->x<=0 && woda->y-this->y>=0){
                 this->predkosc=this->predkosc-1;
                 if(this->predkosc<=0){
                     qDebug() << "GÓRA Prawy";
                     this->y=this->y-1;
                     this->x=this->x+1;
                     this->predkosc=2;
                 }
        }
        if(abs(woda->x-this->x)<60 && abs(woda->y-this->y)<45 && woda->x-this->x<=0 && woda->y-this->y<=0){
                 this->predkosc=this->predkosc-1;
                 if(this->predkosc<=0){
                     qDebug() << "DÓŁ prawy";
                     this->y=this->y+1;
                     this->x=this->x+1;
                     this->predkosc=2;
                 }
        }
        if(abs(woda->x-this->x)<60 && abs(woda->y-this->y)<45 && woda->x-this->x>=0 && woda->y-this->y<=0){
                 this->predkosc=this->predkosc-1;
                 if(this->predkosc<=0){
                     this->y=this->y+1;
                     qDebug() << "dół LEWY";
                     this->x=this->x-1;
                     this->predkosc=2;
                 }
        }

}

void CAmericanSaper::wykrytomine(size_t numer_w_kolejce)
{
    Mapa->delete_object(numer_w_kolejce);

}
void CAmericanSaper::shoot( )
{
    CQBullet *gpocisk=new CQBullet();
    gpocisk->pocisk->set_Mapa(this->Mapa);
    gpocisk->pocisk->x=this->x;
    gpocisk->pocisk->y=this->y;
    gpocisk->pocisk->strona_konfliktu='A';
    gpocisk->pocisk->zasieg=this->zasieg;
    gpocisk->pocisk->obrazenia=this->obrazenia;
    Mapa->add_object(gpocisk,gpocisk->pocisk);
    Mapa->scena->addItem(gpocisk);
}

CAmericanSaper::CAmericanSaper( )
{
   this->x=600;
    this->y=200;
    this->strona_konfliktu='A';
    this->zasieg=300;
    this->predkosc=2;
    this->czas_tworzenia=400;
    this->HP=30;
    this->obrazenia=6;
}




