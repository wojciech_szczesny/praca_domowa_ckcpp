#include "cqwall.h"
#include <QPainter>
#include <QStyleOption>
#include <QtCore/qglobal.h>



CQWall::CQWall( QGraphicsScene *scena )
{
     sciana = new CWall();
     setPos(sciana->x, sciana->y);
     scena->addItem(this);
}

void CQWall::point()
{
    setPos(sciana->x, sciana->y);
}

QRectF CQWall::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust,
                  36 + adjust, 60 + adjust);
}


QPainterPath CQWall::shape() const
{
    QPainterPath path;
    path.addRect(-20, -4, 40, 8);
    return path;
}


void CQWall::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
     painter->setBrush(Qt::white);
    painter->drawRect(-20, -4, 40, 8);
}
