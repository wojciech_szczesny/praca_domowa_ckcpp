#include <QDebug>
#include "cqamericansoldier.h"
#include <QGraphicsView>
#include <QBrush>
#include <QGraphicsRectItem>
#include <QPainter>
#include <QStyleOption>
#include <QtCore/qglobal.h>


CQAmericanSoldier::CQAmericanSoldier( QGraphicsScene *scena )
{
     soldier = new CAmericanSoldier();
     setPos(soldier->x, soldier->y);
     scena->addItem(this);
}

void CQAmericanSoldier::point()
{
    setPos(soldier->x, soldier->y);
}

QRectF CQAmericanSoldier::boundingRect() const
{
    qreal adjust = 0.5;
    return QRectF(-18 - adjust, -22 - adjust,
                  36 + adjust, 60 + adjust);
}


QPainterPath CQAmericanSoldier::shape() const
{
   QPainterPath path;
   path.addRect(-10, -20, 20, 40);
   return path;
}


void CQAmericanSoldier::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(Qt::yellow);
    painter->drawEllipse(-5, -5, 10, 10);
}



CQAmericanSoldier::~CQAmericanSoldier()
{

}

