#ifndef CBUILDING_H
#define CBUILDING_H
/**
* \file cbuilding.h
* \brief Plik nagłówkowy modułu CBuilding.
*
*/
#include "cimmoveable.h"

/** \brief Budynek
 *
* \param HP "życie" budynku
* \param a szerokosc
* \param b wysokosc
*/

class CBuilding : public CImmoveable
{
public:
    int HP;
    int a,b;
    CBuilding();
    virtual void action();
};

#endif // CBUILDING_H
