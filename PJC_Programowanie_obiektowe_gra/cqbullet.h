#ifndef CQBULLET_H
#define CQBULLET_H
#include "cbullet.h"
#include <QGraphicsRectItem>
#include <cqobject.h>


/** \brief Klasa CQBullet.
 *
* Klasa graficzna pocisku. Na mapie malutkie czerwone kółko.
*/
class CQBullet :public QGraphicsRectItem, public CQObject
{
 //   Q_OBJECT
public:
    ///Wsaźnik na element logiczny CBullet
    CBullet *pocisk;
    CQBullet();
    ~CQBullet();
    /// Metoda odpowiedzialna za odświeżanie położenia elementu na mapie
    void point();

    /// Metoda zwraca oszacowanie obszaru namalowanego przez element
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    /// Metodę implementujemy, aby zwrócić dokładny kształt naszego elementu myszy;
    QPainterPath shape() const Q_DECL_OVERRIDE;
    /// Metoda implementuje rzeczywisty obraz
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) Q_DECL_OVERRIDE;
};

#endif // CQBULLET_H
