/*
 W ramach repozytorium o projekt i implementację prostej klasy Vector3D.
 Klasa ma przechowywać współrzędne x,y,z wektora i umożliwiać dodawanie wektorów,
 iloczyn skalarny, iloczyn wektorowy. Dostęp do współrzędnych przez settery i gettery.
 Dobry kostruktor i destruktor.

 Klasa powinna umieć zwrócić std::string w postaci (x,y,z) za pomocą funkcji print()
*/

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
using namespace std;

struct invalid_variable : std::invalid_argument
{
    using invalid_argument::invalid_argument;
};

class Vector3D{
private:
    int x,y,z;
public:
    int calculate_scalar_product(Vector3D);
    Vector3D add_vectors(Vector3D);
    Vector3D calculate_vector_product(Vector3D);
    void set_x(int);
    void set_y(int);
    void set_z(int);
    int get_x();
    int get_y();
    int get_z();
    void print();
    Vector3D(int, int, int);
    Vector3D();
    ~Vector3D();
};
Vector3D::~Vector3D() {
   //Zadanie wymagało stworzenie destruktora, w moim przypadku nie mam żadnych zmiennych dynamicznych,będzie pusty
}
Vector3D::Vector3D() {
    this->x=0;
    this->y=0;
    this->z=0;
}
Vector3D::Vector3D(int a, int b, int c) {
    this->x=a;
    this->y=b;
    this->z=c;
}
Vector3D Vector3D::calculate_vector_product(Vector3D v) {
    Vector3D output;
    try {
        output.x = this->y * v.z - this->z * v.y;
        output.y = this->x * v.z - this->z * v.x;
        output.z = this->x * v.y - this->y * v.x;
    }catch(std::runtime_error){
        cerr << "Blad w bloku w obliczeniach iloczynu wektorowego";
    }
    return output;
}
int Vector3D::calculate_scalar_product(Vector3D a){
    return ( x * a.x ) + ( y * a.y ) + ( z * a.z );
}
void Vector3D::set_x(int a) {
    x = a;
}
void Vector3D::set_y(int a) {
    y = a;
}
void Vector3D::set_z(int a) {
    z = a;
}
int Vector3D::get_x() {
    return (x);
}
int Vector3D::get_y() {
    return (y);
}
int Vector3D::get_z() {
    return (z);
}
Vector3D Vector3D::add_vectors( Vector3D input_second) {
    Vector3D v_output;
    v_output.x=this->x+input_second.x;
    v_output.y=this->y+input_second.y;
    v_output.z=this->z+input_second.z;
    return (v_output);
}
void Vector3D::print() {
    std::ostringstream oss;
    oss << "(" << this->x << "," << this->y << "," << this->z << ")";
    std::string var = oss.str();
    cout << var;
}

int main() {
    Vector3D first_vector;
    int zmienna_x;
    cout << "Podaj zmienna_x dla wektora: " << endl;
    cin >> zmienna_x;
    //Wyjatkowa sytuacja, gdy zmienne podawane do wektora są wczytywane z konsoli istnieje wtedy ryzyko podania niewlasciwych danych.
    if (!cin.good())
        throw invalid_variable("Please give the variable type int ");
    first_vector.set_x(zmienna_x);
    first_vector.set_y(2);
    first_vector.print();
    Vector3D second_vector(1,9,1);
    second_vector.print();
    Vector3D sum_of_two_vector;
    sum_of_two_vector=first_vector.add_vectors(second_vector);
    sum_of_two_vector.print();
    cout << endl << first_vector.calculate_scalar_product(second_vector) << endl;
    Vector3D vector_product = first_vector.calculate_vector_product(second_vector);
    vector_product.print();
    return 0;
}