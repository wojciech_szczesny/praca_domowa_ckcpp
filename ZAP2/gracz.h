#ifndef FUNKCJE_H
#define FUNKCJE_H
#include <string>
#include <iostream>
#include "historia.h"
#include "wyswietl.h"
using namespace std; //tutaj definiujemy przestrzeń nazw - dostępną globalnie

struct Gracz
{
 string nick;
 Gracz *nextG;
 Wspolrzedne *nextW;
};

void wyswietlListeGraczy(Gracz *&g);
void wyswietlHistorie(Gracz *&glowa);

Gracz* dodajGracza(Gracz *&g, string nick); //tu tylko deklaracja funkcji, czyli nagłówek i średnik

void usunListe(Gracz *&g);
#endif  // FUNKCJE



