#ifndef PLANSZA_H
#define PLANSZA_H
#include "wyswietl.h"

enum pole
{
    kolko = 0,
    krzyzyk,
    puste,
};

struct plansza
{
    int rozmiar;
    pole **wskaznik;
};
plansza *tworzPlansze( int n );
void usun_plansze(plansza*);
void wyswietl(plansza*);
#endif // PLANSZA_H
