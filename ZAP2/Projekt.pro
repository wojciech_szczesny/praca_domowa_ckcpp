TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    menu.cpp \
    plansza.cpp \
    gracz.cpp \
    nowagra.cpp \
    historia.cpp \
    wyswietl.cpp

HEADERS += \
    gracz.h \
    historia.h \
    menu.h \
    nowagra.h \
    plansza.h \
    wyswietl.h

