#ifndef HISTORIA_H
#define HISTORIA_H
#include<string>
#include <iostream>
#include "wyswietl.h"


using namespace std;


struct Wspolrzedne
{
 string nick;
 int x;
 int y;
 Wspolrzedne *next;
};

void wyswietl(Wspolrzedne *&glowa);
Wspolrzedne *dodajWspolrzedne(string nick, int x, int y); //tu tylko deklaracja funkcji, czyli nagłówek i średnik
void dodajPozycje(string tekst, Wspolrzedne *&glowa, int x, int y);


#endif // HISTORIA

