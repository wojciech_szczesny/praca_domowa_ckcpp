#include "gracz.h"


using namespace std;

Gracz* dodajGracza(Gracz *&g, string nick)
{
    Gracz *aktualny = g, *tmp;
    if (g == NULL) {
        g = new Gracz;
        g->nextG = NULL;
        g->nextW = NULL;
        g->nick = nick;

        return g;
    }
    else {
        aktualny = g;
        while (aktualny->nextG != NULL) {
            aktualny=aktualny->nextG;
        }

        tmp = new Gracz;
        tmp->nextG=NULL;
        tmp->nextW = NULL;
        tmp->nick=nick;
        aktualny->nextG=tmp;
    }

    return tmp;



}
void usunListe(Gracz *&g){

    Gracz  *tmpG;
    Wspolrzedne *tmpW;
    while (g!= NULL)  {
        tmpG=g;
        while(g->nextW!=NULL){
            tmpW=g->nextW;
            g->nextW=g->nextW->next;
            delete tmpW;
        }
        g=g->nextG;
        delete tmpG;
      }
}

void wyswietlListeGraczy(Gracz *&g)
{
    wyswietlcout("\n*******************************************************************************");
    wyswietlcout (" Lista graczy: \n");
    Gracz *tmp=g;
     int licznik=1;
     while (tmp!=NULL) {                      // dopóki to nie koniec listy
     wyswietlcoutint(licznik);
     wyswietlcout(": ");
     wyswietlcout(tmp->nick);
     wyswietlcout("\n");   // wypisujemy zawartość elementu
     tmp = tmp->nextG;                        // i przechodzimy do el. następnego
     licznik++;
     }
     wyswietlcout("\n*******************************************************************************");;
}
void wyswietlHistorie(Gracz *&glowa){
    //wyswietlcout("Podaj nick gracza do wyswietlenia jego historii. ");
    //string tekst;
    //tekst=wczytajstring();
    //wyswietlcout("Historia posuniec: ");wyswietlcout(tekst); wyswietlcout("\n");

    // funkcja bedzie rozbudowana o przeszukiwanie listy wg. nazw... i dopiero wyswietlanie historii gry...
    wyswietl(glowa->nextW);
}


