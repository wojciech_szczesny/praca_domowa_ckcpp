#ifndef WYSWIETL_H
#define WYSWIETL_H
#include <string>
#include <iostream>
using namespace std;
void wyswietlcout(string tekst);
void wyswietlcoutint(int liczba);
int wczytajint();
char wczytajchar();
string wczytajstring();

#endif // WYSWIETL

