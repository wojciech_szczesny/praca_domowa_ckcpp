#ifndef NOWAGRA_H
#define NOWAGRA_H
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "nowagra.h"
#include "gracz.h"
#include "plansza.h"
#include "wyswietl.h"
#include "historia.h"


int nowa_gra_vs_user( Gracz *nick1, Gracz *nick2, plansza *plansz);
pole sprawdzeniepoziom(plansza *plansz);
pole sprawdzeniepion(plansza *plansz);
pole sprawdzenieukos1(plansza *plansz);
pole sprawdzenieukos2(plansza *plansz); //brak

#endif // NOWAGRA_H
