#ifndef MENU_H
#define MENU_H
#include <fstream>
#include "gracz.h"
#include "plansza.h"
#include "nowagra.h"
#include "historia.h"
#include "wyswietl.h"



void menuGlowne(Gracz *&g);
Gracz *dodajGracza(Gracz *&g);
plansza *stworzPlansze();
void zapiszDoPliku(Gracz *g);

#endif // MENU

