#include "historia.h"

Wspolrzedne *dodajWspolrzedne(string nick, int x, int y)
{
    Wspolrzedne *glowa1;
        glowa1=new Wspolrzedne;
        glowa1->x = x;
        glowa1->y = y;
        glowa1->nick=nick;
        glowa1->next = NULL;
    return glowa1;
}

void dodajPozycje(string nick, Wspolrzedne *&glowa, int x, int y){

    Wspolrzedne *tmp,*aktualny;
    tmp=new Wspolrzedne;
            tmp->next=NULL;
            tmp->x = x;
            tmp->y = y;
            tmp->nick=nick;
            aktualny=glowa;
            while (aktualny->next!= NULL) {
                  aktualny= aktualny->next;
                    }
            aktualny->next=tmp;

}

void wyswietl(Wspolrzedne *&glowa){      //wyswietla wspolrzedne punktow, w historii
    wyswietlcout("\n*******************************************************************************");
    wyswietlcout (" Historia: \n");

        Wspolrzedne *aktualny, *poprzedni;
        aktualny=glowa;
        int licznik=1;
        while (aktualny!=NULL) {         //wyswietla numer ruchu: wspolrzedne(x,y) i nazwe gracza, ktory robil ruch
            wyswietlcoutint(licznik);
            wyswietlcout(": (");
            wyswietlcoutint(aktualny->x);
            wyswietlcout(",");
            wyswietlcoutint(aktualny->y);
            wyswietlcout(") ");
            wyswietlcout(aktualny->nick);
            wyswietlcout("\n");
            licznik++;
            poprzedni=aktualny;
            aktualny=aktualny->next;
        }
     wyswietlcout("\n*******************************************************************************");;
}
